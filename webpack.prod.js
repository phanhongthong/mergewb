var webpack = require("webpack");
var path = require("path");
var common = require("./webpack.common");
var merge = require("webpack-merge");

var BUILD_DIR = path.join(__dirname, "dist");

var config = {
  mode: "production",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].[chunkhash].js",
    publicPath: "/"
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV)
    })
  ]
};

module.exports = merge(common, config);
